# This is my first heading

## This is my second heading
This heading is like a h2

### This is my third heading
This one is like a h3

[This is a link to a nice song](https://www.youtube.com/watch?v=SOtd0_tSwMA&list=RDwZN1HKn3Qus&index=3)

<br>
<br>

### This is a ### heading for this picture of a pope 
![this is a picture](bless.png) 

<br>
<br>

| Tables   |      Are      | Very  |  Cool |
|----------|:-------------:|:-----:|------:|
|left      | center        |center | rigth |
| Soooooo  | thiiis        | iis   | pretty|
| |  | ||


### Unordered lists, that's a way of listing something unordered, yay
- this is how we do it
    - this is how we do it, tab once
        - this is how we do it, tab twice

### Ordered list are easy
1. just start me here by 1 followed by a . 
2. same here just with 2 and a .
3. and 
4. so
5. on 
6. we go along as much as we need
    1. but wait, here i need a tab
7. but then we can go along again

### **Is bold better for a heading?**

### *Is italic maybe better?*

### ***How about both!***

<br>

### html 

```html
        <html>
          <head>
            <title>I'm a code block</title>
          </head>
```


> This is a blockqoute i'm made with this one >
>> I'm nested bc of this >>

        