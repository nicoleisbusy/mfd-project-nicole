const app = Vue.createApp({
 data()  {
     return{
         showBooks: true,
         title: 'The Final Empire', 
         author: 'Brandon Sanderson',
         age: '1',
         x: 0,
         y: 0
         
     }
   },
   methods: {
      changeTitle() {
       console.log("You clicked me")   
       this.title = 'I am a new title'
      },
     toggleShowBooks () {
        this.showBooks = !this.showBooks
     },
     handleEvent(e, data){
         console.log(e, e.type)
         if (data){
             console.log(data)
         }
     },
     handleMousemove(e) {
         this.x = e.offsetX
         this.y = e.offsetY
     }
   } 
})

app.mount('#hej')

