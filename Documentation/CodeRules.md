# Vue3 Code Rules


General Rules. 

1. When commenting on a commit or in vue, it must be in english. 
2. Use "#" when commiting with the issue and add issues number from gitlab, finish with a descriptive msg about what you were doing. Like "#48 making comments in the code"
3. Install prettier and eslint plugin to vs code. 
4. Keep npm Packages Updated
5. Manage Global File for Shared Variable
6. Clean Code and Refactoring


**Code Best Practices**

CodeRules Categories A-B-C-D:

4. A - Use multi-word component names.
5. A - Use detailed prop definitions.
6. A - Use *:key* inside v-for
7. A - Avoid v-if with v-for
8. B - Whenever a build system is available to concatenate files, each component should be in its own file.

    ```
    components/
    |- TodoList.js
    |- TodoItem.js

    ```

    ```
    components/
    |- TodoList.vue
    |- TodoItem.vue
    ```
9. B - Filenames of Single-File Components should either be always **PascalCase** or always kebab-case.
And **Base** components should be prefixed accordingly. Feks Basebutton.vue, BaseIcon.vue, BaseHeading.vue

10. B - Components that should only ever have a single active instance should begin with the **The** prefix, to denote that there can be only one.

    ```
    components/
    |- TheHeading.vue
    |- TheSidebar.vue
    ```
11. Child components that are tightly coupled with their parent should include the parent component name as a prefix.

    ```
    components/
    |- TodoList.vue
    |- TodoListItem.vue
    |- TodoListItemButton.vue
    ```
    ```
    components/
    |- SearchSidebar.vue
    |- SearchSidebarNavigation.vue
    ```

12. Component names should start with the highest-level (often most general) words and end with descriptive modifying words.

    ```
    components/
    |- SearchButtonClear.vue
    |- SearchButtonRun.vue
    |- SearchInputQuery.vue
    |- SearchInputExcludeGlob.vue
    |- SettingsCheckboxTerms.vue
    |- SettingsCheckboxLaunchOnStartup.vue
    ```

13. Use kebab-case for events

    ```jsx
    js
    this.$emit("close-window");

    vue
    <popup-window @close-window='handleEvent()' />
    ```
14. Data should always return a function

    ```jsx
      export default {
        data() {
          return {
            name: "My Window",
            articles: [],
          };
        },
      };
    ```

15. Stay consistent with your directive shorthand

    ```jsx
    @ is short for v-on:

    : is short for v-bind

    # is short for v-slot
    ``` 
16. Don’t call a method on created AND watch

17. Fundamental security rule when using Vue is never use non-trusted content as your component template.

18. When working with primitives, always go with **ref()**. When working with objects, go with **reactive()**
    ```jsx
    //When working with primitives like string, numbers etc:
    const myName = ref('Nicky')
    //When working with objects:
    const user = reactive({
      name: 'Nicky',
      age: 37
    });
    ```
19. Computed functions are also super powerful when you eg: need to output a list of items that can see filtered:

    ```jsx
    const filteredBlogPosts = computed(() => {
      return blogPosts.value.filter((blogPost) => {
        return blogPost.title.toLowerCase().includes(searchString.value.toLowerCase())
      })
    })
    ``` 
20. Avoid directly DOM manipulation

21. Use v-deep to overwrite styles
22. Dont mutate props
23. Bind style directly to state / data
    ```jsx
    const color = ref('#f000');
    <style>
    .text {
      color: v-bind(color); 
    }
    </style>
    ```
24. Vue Component Reusability & Communication
25. Template Expressions should only have Basic JavaScript Expressions
26. Avoid Multiple V-Condition.
27. Routing allows the user to switch between pages without refreshing the page. This is what makes navigation easy and really nice in your web applications.



## Vue 3 CodeRules Categories A-B-C-D:

<details>
<summary><b><u>Rule A - Væsentlige</u></b></summary>


*Dette er den officielle style guide til Vue-specifik kode.*

Description: Disse regler hjælper med at forhindre fejl, så lær og overhold dem for enhver pris. Undtagelser kan eksistere, men bør være meget sjældne og kun foretages af dem med ekspertviden om både JavaScript og Vue.

[Rules Essential Link](https://vuejs.org/style-guide/rules-essential.html)


**Brug komponentnavne med flere ord**

Hvorfor: Bruger Komponentnavne skal altid være flere ord, undtagen rod komponenter App. Dette [forhindrer konflikter](https://html.spec.whatwg.org/multipage/custom-elements.html#valid-custom-element-name) med eksisterende og fremtidige HTML-elementer, da alle HTML-elementer er et enkelt ord.


```
<!-- in pre-compiled templates -->
<TodoItem />

<!-- in in-DOM templates -->
<todo-item></todo-item>
```



**Brug detaljerede prop definitioner**

Godt: I committed kode bør prop definitioner altid være så detaljerede som muligt, idet de mindst specificerer type(r).


```
<!-- js-->
props: {
  status: String
}

```



```
// Even better! js fil
props: {
  status: {
    type: String,
    required: true,

    validator: value => {
      return [
        'syncing',
        'synced',
        'version-conflict',
        'error'
      ].includes(value)
    }
  }
}

```



**Brug nøgle v-for**

godt: **key **med **v-for** er altid påkrævet på komponenter, for at opretholde den interne komponent tilstand nede i undertræet. Selv for elementer er det dog en god praksis at opretholde en forudsigelig adfærd, såsom [objektkonstans](https://bost.ocks.org/mike/constancy/) i animationer.


```
<!-- template -->
<ul>
  <li
    v-for="todo in todos"
    :key="todo.id"
  >
    {{ todo.text }}
  </li>
</ul>

```



**Undgå v-if med v-for**

Brug aldrig v-if på samme element som v-for.


```
<!-- template -->
<ul>
  <li
    v-for="user in activeUsers"
    :key="user.id"
  >
    {{ user.name }}
  </li>
</ul>
```



```
<ul>
  <template v-for="user in users" :key="user.id">
    <li v-if="user.isActive">
      {{ user.name }}
    </li>
  </template>
</ul>
```
**Brug komponent-omfanget styling**

For applikationer kan stilarter i en komponent på øverste niveau App Og i layout komponenter være globale, men alle andre komponenter bør altid være omfattet.

Dette er kun relevant for [enkeltfilskomponenter](https://vuejs.org/guide/scaling-up/sfc.html) . Det kræver ikke , at [scopedattributten](https://vue-loader.vuejs.org/en/features/scoped-css.html) bruges. Scoping kan være gennem [CSS-moduler](https://vue-loader.vuejs.org/en/features/css-modules.html) , en klassebaseret strategi såsom [BEM](http://getbem.com/) eller et andet bibliotek/konvention.

**Komponent Biblioteker bør dog foretrække en klassebaseret strategi i stedet for at bruge scopedattributten.


```
<template>
  <button class="button button-close">×</button>
</template>

<!-- Using the `scoped` attribute -->
<style scoped>
.button {
  border: none;
  border-radius: 2px;
}

.button-close {
  background-color: red;
}
</style>
```



```
<template>
  <button :class="[$style.button, $style.buttonClose]">×</button>
</template>

<!-- Using CSS modules -->
<style module>
.button {
  border: none;
  border-radius: 2px;
}

.buttonClose {
  background-color: red;
}
</style>

```



```
<template>
  <button class="c-Button c-Button--close">×</button>
</template>

<!-- Using the BEM convention -->
<style>
.c-Button {
  border: none;
  border-radius: 2px;
}

.c-Button--close {
  background-color: red;
}
</style>

```
**Undgå at udsætte private funktioner i mixins**

Brug altid $_præfikset for tilpassede private egenskaber i et plugin, mixin osv., der ikke bør betragtes som offentlig API. For at undgå konflikter med kode fra andre forfattere, skal du også inkludere et navngivet omfang (f.eks $_yourPluginName_. ).

js fil


```
const myGreatMixin = {
  // ...
  methods: {
    $_myGreatMixin_update() {
      // ...
    }
  }
}

```



```
// Even better!
const myGreatMixin = {
  // ...
  methods: {
    publicMethod() {
      // ...
      myPrivateFunction()
    }
  }
}

function myPrivateFunction() {
  // ...
}

export default myGreatMixin
```
</details>


<details>
<summary><b><u>Rule B - Stærkt anbefalet</u></b></summary>


Description: Disse regler har vist sig at forbedre læsbarheden og/eller udvikler oplevelsen i de fleste projekter. Din kode vil stadig køre, hvis du overtræder dem, men overtrædelser bør være sjældne og velbegrundede.

[Rules Strongly Recommended Link](https://vuejs.org/style-guide/rules-strongly-recommended.html)


**Komponent Filer**

Når et byggesystem er tilgængeligt til at sammenkæde filer, skal hver komponent være i sin egen fil.


```
components/
|- TodoList.js
|- TodoItem.js
```



```
components/
|- TodoList.vue
|- TodoItem.vue
```



** ** Enkelt-fil komponent filnavn casing

** Filnavne på enkeltfilskomponenter skal enten altid være PascalCase eller altid kebab-case.

PascalCase fungerer bedst med autofuldførelse i kode editorer, da det stemmer overens med, hvordan vi refererer til komponenter i JS(X) og skabeloner, hvor det er muligt. Men filnavne med blandede store og små bogstaver kan nogle gange skabe problemer på filsystemer, der ikke er følsomme over for store og små bogstaver, hvorfor kebab-case også er helt acceptabel.


```
components/
|- MyComponent.vue

components/
|- my-component.vue
```



**Navne på basiskomponenter**

Basiskomponenter (alias præsentations-, dumme eller rene komponenter), der anvender app-specifik stil og konventioner, bør alle begynde med et specifikt præfiks, såsom Base, App, eller V.


```
components/
|- BaseButton.vue
|- BaseTable.vue
|- BaseIcon.vue
```



```
components/
|- AppButton.vue
|- AppTable.vue
|- AppIcon.vue
```



```
components/
|- VButton.vue
|- VTable.vue
|- VIcon.vue
```



**Navne på enkeltforekomster**

Komponenter, der kun bør have en enkelt aktiv instans, skal begynde med Thepræfikset for at angive, at der kun kan være én.

Dette betyder ikke, at komponenten kun bruges på en enkelt side, men den vil kun blive brugt én gang pr. side . Disse komponenter accepterer aldrig nogen rekvisitter, da de er specifikke for din app, ikke deres kontekst i din app. Hvis du finder behovet for at tilføje rekvisitter, er det en god indikation af, at dette faktisk er en genbrugelig komponent, der kun bruges én gang pr. side indtil videre .


```
Dårlig
components/
|- Heading.vue
|- MySidebar.vue


Godt: 
components/
|- TheHeading.vue
|- TheSidebar.vue

```



**Tæt koblede komponentnavne**

Underordnede komponenter, der er tæt koblet til deres overordnede, skal indeholde det overordnede komponentnavn som et præfiks.

Hvis en komponent kun giver mening i sammenhæng med en enkelt overordnet komponent, bør denne sammenhæng være tydelig i dens navn. Da redaktører typisk organiserer filer alfabetisk, holder dette også disse relaterede filer ved siden af ​​hinanden.


```
components/
|- TodoList.vue
|- TodoListItem.vue
|- TodoListItemButton.vue

components/
|- SearchSidebar.vue
|- SearchSidebarNavigation.vue
```

**Ordenes rækkefølge i komponentnavne**

Komponentnavne skal starte med det højeste niveau (ofte mest generelle) ord og slutte med beskrivende modificerende ord.


```
components/
|- SearchButtonClear.vue
|- SearchButtonRun.vue
|- SearchInputQuery.vue
|- SearchInputExcludeGlob.vue
|- SettingsCheckboxTerms.vue
|- SettingsCheckboxLaunchOnStartup.vue

```

**Selvlukkende komponenter**

Komponenter uden indhold bør være selvlukkende i [Single-File Components](https://vuejs.org/guide/scaling-up/sfc.html) , strengskabeloner og [JSX](https://vuejs.org/guide/extras/render-function.html#jsx-tsx) - men aldrig i DOM-skabeloner.

Komponenter, der lukker sig selv, kommunikerer, at de ikke kun ikke har noget indhold, men er beregnet til ikke at have noget indhold. Det er forskellen mellem en tom side i en bog og en mærket "Denne side er med vilje efterladt tom." Din kode er også pænere uden det unødvendige lukkemærke.

Desværre tillader HTML ikke, at tilpassede elementer er selvlukkende - kun [officielle "ugyldige" elementer](https://www.w3.org/TR/html/syntax.html#void-elements) . Det er grunden til, at strategien kun er mulig, når Vues skabelonkompiler kan nå skabelonen før DOM og derefter tjene den DOM spec-kompatible HTML.


```
template
<!-- In Single-File Components, string templates, and JSX -->
<MyComponent/>

<!-- In DOM templates -->
<my-component></my-component>
```

**Komponentnavnskasse i skabeloner**

I de fleste projekter bør komponentnavne altid være PascalCase i [Single-File Components](https://vuejs.org/guide/scaling-up/sfc.html) og strengskabeloner - men kebab-case i DOM-skabeloner.

PascalCase har et par fordele i forhold til kebab-etui:

* Redaktører kan autofuldføre komponentnavne i skabeloner, fordi PascalCase også bruges i JavaScript.
* &lt;MyComponent>er mere visuelt adskilt fra et enkeltords HTML-element end &lt;my-component>, fordi der er to tegnforskelle (de to store bogstaver) i stedet for kun én (en bindestreg).
* Hvis du bruger ikke-Vue tilpassede elementer i dine skabeloner, såsom en webkomponent, sikrer PascalCase, at dine Vue-komponenter forbliver tydeligt synlige.

_Desværre, på grund af HTML's ufølsomhed over for store og små bogstaver, skal DOM-skabeloner stadig bruge kebab-case._


```
<!-- In Single-File Components and string templates -->
<MyComponent/>

<!-- In DOM templates -->
<my-component></my-component>
ELLER

<!-- Everywhere →
<my-component></my-component>
```

**Komponentnavnshus i JS/JSX**

Komponentnavne i JS/ [JSX](https://vuejs.org/guide/extras/render-function.html#jsx-tsx) bør altid være PascalCase, muligheden kan være kebab-case inde i strenge til enklere applikationer, der kun bruger global komponentregistrering gennem app.component.


```
// js
app.component('MyComponent', {
  // ...
})

// js
app.component('my-component', {
  // ...
})

 // js
import MyComponent from './MyComponent.vue'

// js
export default {
name: 'MyComponent'
  // ...
}

```



**Navne på komponenter i fulde ord**

Komponentnavne bør foretrække hele ord frem for forkortelser.

_Autofuldførelsen i redaktører gør omkostningerne ved at skrive længere navne meget lave, mens den klarhed, de giver, er uvurderlig. Især usædvanlige forkortelser bør altid undgås._


```
components/
|- StudentDashboardSettings.vue
|- UserProfileOptions.vue
```



**Prop navn casing**

Propnavne skal altid bruge camelCase under erklæringen, men kebab-case i skabeloner og [JSX](https://vuejs.org/guide/extras/render-function.html#jsx-tsx) 

Vi følger simpelthen konventionerne for hvert sprog. Inden for JavaScript er camelCase mere naturligt. Inden for HTML er kebab-case.


```
// js
props: {
  greetingText: String
}
```



```
// template
<WelcomeMessage greeting-text="hi"/>
```



**Multi-attribut elementer**

Elementer med flere attributter skal strække sig over flere linjer med én attribut pr. linje.

I JavaScript er opdeling af objekter med flere egenskaber over flere linjer almindeligt betragtet som en god konvention, fordi det er meget nemmere at læse. Vores skabeloner og [JSX](https://vuejs.org/guide/extras/render-function.html#jsx-tsx) fortjener samme overvejelse.


```
<img
  src="https://vuejs.org/images/logo.png"
  alt="Vue Logo"
>
```


```
<MyComponent
  foo="a"
  bar="b"
  baz="c"
/>
```



**Simple udtryk i skabeloner**

Komponentskabeloner bør kun indeholde simple udtryk, med mere komplekse udtryk omdannet til beregnede egenskaber eller metoder.

Komplekse udtryk i dine skabeloner gør dem mindre deklarative. Vi bør stræbe efter at beskrive, hvad der skal vises, ikke hvordan vi beregner den værdi. Beregnet egenskaber og metoder gør det også muligt at genbruge koden.


```
<!-- In a template -->
{{ normalizedFullName }}
```



```
// The complex expression has been moved to a computed property
computed: {
  normalizedFullName() {
    return this.fullName.split(' ')
      .map(word => word[0].toUpperCase() + word.slice(1))
      .join(' ')
  }
}
```

**Simple beregnede egenskaber**

Komplekse beregnede egenskaber bør opdeles i så mange enklere egenskaber som muligt.


```
computed: {
  basePrice() {
    return this.manufactureCost / (1 - this.profitMargin)
  },

  discount() {
    return this.basePrice * (this.discountPercent || 0)
  },

  finalPrice() {
    return this.basePrice - this.discount
  }
}
```

**Angivne attributværdier**

Ikke-tomme HTML-attributværdier skal altid være inden for anførselstegn (enkelt eller dobbelt, alt efter hvad der ikke bruges i JS).


```
// template
<input type="text">
```


```
// template
<AppSidebar :style="{ width: sidebarWidth + 'px' }">
```



**Direktivet stenografi**

Direktiv stenografi ( : for v-bind:, @ for v-on:og # for v-slot) bør altid eller aldrig bruges.

// template 


```
<input
  :value="newTodoText"
  :placeholder="newTodoInstructions"
>
```



```
<input
  v-bind:value="newTodoText"
  v-bind:placeholder="newTodoInstructions"
>
```



```
<input
  @input="onInput"
  @focus="onFocus"
>
```



```
<input
  v-on:input="onInput"
  v-on:focus="onFocus"
>
```



```
<template v-slot:header>
  <h1>Here might be a page title</h1>
</template>

<template v-slot:footer>
  <p>Here's some contact info</p>
</template>
```



```
<template #header>
  <h1>Here might be a page title</h1>
</template>

<template #footer>
  <p>Here's some contact info</p>
</template>
```

</details>

<details>
<summary><b><u>Rule C - Anbefalet</u></b></summary>


Description: I disse regler beskriver vi hver acceptabel mulighed og foreslår et standardvalg. Det betyder, at du er velkommen til at træffe et andet valg i din egen kodebase, så længe du er konsekvent og har en god grund.

[Rules Recommended Link](https://vuejs.org/style-guide/rules-recommended.html)


**Komponent-/instansoptionsrækkefølge**

*Komponent/instans-indstillinger skal bestilles konsekvent.*

Dette er standard rækkefølgen, vi anbefaler for komponent indstillinger. De er opdelt i kategorier, så du ved, hvor du kan tilføje nye egenskaber fra plugins.
```
1. Global bevidsthed (kræver viden ud over komponenten)
    1. name
2. Skabelonkompilerindstillinger (ændrer den måde, skabeloner kompileres på)
    2. compilerOptions
3. Skabelonafhængigheder (aktiver brugt i skabelonen)
    3. components
    4. directives
4. Sammensætning (fletter egenskaber ind i mulighederne)
    5. extends
    6. mixins
    7. provide/inject
5. Interface (grænsefladen til komponenten)
    8. inheritAttrs
    9. props
    10. emits
6. Composition API (indgangspunktet for brug af Composition API)
    11. setup
7. Lokal stat (lokale reaktive egenskaber)
    12. data
    13. computed
8. Hændelser (tilbagekald udløst af reaktive hændelser)
    14. watch
    15. Livscyklushændelser (i den rækkefølge, de kaldes)
        1. beforeCreate
        2. created
        3. beforeMount
        4. mounted
        5. beforeUpdate
        6. updated
        7. activated
        8. deactivated
        9. beforeUnmount
        10. unmounted
        11. errorCaptured
        12. renderTracked
        13. renderTriggered
9. Ikke-reaktive egenskaber (forekomstegenskaber uafhængige af reaktivitetssystemet)
    16. methods
10. Gengivelse (den deklarative beskrivelse af komponentoutputtet)
    17. template/render

```

**Element attribut rækkefølge**

*Attributterne for elementer (inklusive komponenter) bør ordnes konsekvent.*

Dette er standard rækkefølgen, vi anbefaler for komponentindstillinger. De er opdelt i kategorier, så du ved, hvor du skal tilføje tilpassede attributter og direktiver.

```
1. Definition (giver komponentindstillingerne)
    1. is
2. Liste-gengivelse (opretter flere variationer af det samme element)
    2. v-for
3. Betingelser (om elementet er gengivet/vist)
    3. v-if
    4. v-else-if
    5. v-else
    6. v-show
    7. v-cloak
4. Gengivelsesmodifikatorer (ændrer den måde, elementet gengives på)
    8. v-pre
    9. v-once
5. Global bevidsthed (kræver viden ud over komponenten)
    10. id
6. Unikke attributter (attributter, der kræver unikke værdier)
    11. ref
    12. key
7. To-vejs binding (kombinerer binding og begivenheder)
    13. v-model
8. Andre attributter (alle uspecificerede bundne og ubundne attributter)
9. Hændelser (komponenthændelseslyttere)
    14. v-on
10. Indhold (tilsidesætter indholdet af elementet)
    15. v-html
    16. v-text

```

**Tomme linjer i komponent-/forekomstindstillinger**

** Du vil måske tilføje en tom linje mellem egenskaber med flere linjer, især hvis indstillingerne ikke længere kan passe på din skærm uden at rulle.

Når komponenter begynder at føles trange eller svære at læse, kan tilføjelse af mellemrum mellem egenskaber med flere linjer gøre dem lettere at skumme igen. I nogle redaktører, såsom Vim, kan formateringsmuligheder som denne også gøre dem nemmere at navigere med tastaturet.


```
// js
props: {
  value: {
    type: String,
    required: true
  },

  focused: {
    type: Boolean,
    default: false
  },

  label: String,
  icon: String
},

computed: {
  formattedValue() {
    // ...
  },

  inputClasses() {
    // ...
  }
}
```



```
// No spaces are also fine, as long as the component
// is still easy to read and navigate.
props: {
  value: {
    type: String,
    required: true
  },
  focused: {
    type: Boolean,
    default: false
  },
  label: String,
  icon: String
},
computed: {
  formattedValue() {
    // ...
  },
  inputClasses() {
    // ...
  }
}

```



**Enkelt-fil komponent elementrækkefølge på øverste niveau**

[Enkeltfilskomponenter](https://vuejs.org/guide/scaling-up/sfc.html) bør altid ordne &lt;script>, &lt;template>, og &lt;style>tags konsekvent med &lt;style>sidst, fordi mindst en af ​​de to andre altid er nødvendig.


```
<!-- ComponentA.vue -->
<script>/* ... */</script>
<template>...</template>
<style>/* ... */</style>

<!-- ComponentB.vue -->
<script>/* ... */</script>
<template>...</template>
<style>/* ... */</style>
```



```
<!-- ComponentA.vue -->
<template>...</template>
<script>/* ... */</script>
<style>/* ... */</style>

<!-- ComponentB.vue -->
<template>...</template>
<script>/* ... */</script>
<style>/* ... */</style>

```

</details>


<details>
<summary><b><u>Rule D - Brug med forsigtighed</u></b></summary>


Description: Disse regler kaster lys over potentielt risikable funktioner og beskriver, hvornår og hvorfor de bør undgås.

[Rules with Caution Link](https://vuejs.org/style-guide/rules-use-with-caution.html)


**Elementvælgere med scoped**

Elementvælgere bør undgås med scoped.

Foretrækker klassevælgere frem for elementvælgere i scope stilarter, fordi et stort antal elementvælgere er langsomme.


```
<template>
  <button class="btn btn-close">×</button>
</template>

<style scoped>
.btn-close {
  background-color: red;
}
</style>

```



**Implicit forældre-barn kommunikation**

*Rekvisitter og begivenheder bør foretrækkes til kommunikation mellem forældre og barn i stedet for this.$parenteller muterende rekvisitter.*

En ideel Vue-applikation er rekvisitter ned, begivenheder op. At holde sig til denne konvention gør dine komponenter meget nemmere at forstå. Men der er kant tilfælde, hvor prop mutation eller this.$parentkan forenkle to komponenter, der allerede er dybt koblet.

Problemet er, at der også er mange simple tilfælde, hvor disse mønstre kan tilbyde bekvemmelighed. Pas på: lad dig ikke forføre til at handle enkelt (at være i stand til at forstå strømmen i din stat) for kortsigtet bekvemmelighed (skrive mindre kode).


```
app.component('TodoItem', {
  props: {
    todo: {
      type: Object,
      required: true
    }
  },

  emits: ['input'],

  template: `
    <input
      :value="todo.text"
      @input="$emit('input', $event.target.value)"
    >
  `
})
```



```
app.component('TodoItem', {
  props: {
    todo: {
      type: Object,
      required: true
    }
  },

  emits: ['delete'],

  template: `
    <span>
      {{ todo.text }}
      <button @click="$emit('delete')">
        ×
      </button>
    </span>
  `
})
```


Add to file eslint.js what the group agrees to use A-B-C -D?


```
  extends: [
    "plugin:vue/essential",
"eslint:strongly recommended",
    "eslint:recommended",
  ],
```

[Style Guide Youtube](https://www.youtube.com/watch?v=FZ0YtjgO0DQ )

[Style Guide Docs](https://vuejs.org/style-guide/)

[Git Flow](https://www.toptal.com/software/trunk-based-development-git-flow)


[Vuejs Best Practices 01](https://learnvue.co/2020/01/12-vuejs-best-practices-for-pro-developers/#9-components-declared-and-used-once-should-have-the-prefix-the)

[Vuejs Best Practices 02](https://medium.com/js-dojo/vue-3-tips-best-practices-54aec91d95dc)

[Vuejs Best Practices 03](https://vuejs.org/guide/best-practices/accessibility.html#content-structure)

[Vetur Formatting Issues](https://lifesaver.codes/answer/formatting-issues-476)

</details>
