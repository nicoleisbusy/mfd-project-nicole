# Vue 3 
*Declarative and component based framework with efficiency in mind*

1. [Quick intro](#intro) <br>
    1. 1 [Core Features](#coreFeatures) <br>

## 1 . Quick intro <a name="intro"></a>

> ### **Quick facts** 
>
><br>
>
> **Creator:** Evan You 
><br>
>**Month of first code commit:** July 2013
><br>
>**First release:** February 25, 2014
><br>
>**Latest release:** 	August 5, 2021 
>
>[Read more here](https://en.wikipedia.org/wiki/Vue.js) <br>
>[See the documentary](https://www.youtube.com/watch?v=OrxmtDw4pVI)

**What is Vue** <br>
The people that developed vue, call it a progressive framework, because it is programmed to grow and adapt with the project. 

**Pro's and con's** <br>

|  Pro | Con  |  
|---|---|
|Lightweight;       |Limitations in making use of the community;|  
|User-friendly;     |Lack of scalability;   |  
|Beginner-friendly; |Lack of plugins;   | 
|Reusable;          |Lack of highly experienced experts;   |
|Ease of use;       |Difficulties with mobile support;   |
|Virtual DOM;       |Difficulties with two-way binding;   |
|Segregation;       | Excessive code flexibility.-   |
|Integration;       |   |
|Compatibility;     |   |
|Customization.     |   |

[Read the details of pro's and cons here](https://ddi-dev.com/blog/programming/the-good-and-the-bad-of-vue-js-framework-programming/)

**There's a lot of ways to use vue:** 
- Enhancing static HTML without a build step
- Embedding as Web Components on any page
- Single-Page Application (SPA)
- Fullstack / Server-Side-Rendering (SSR)
- Jamstack / Static-Site-Generation (SSG)
- Targeting desktop, mobile, WebGL or even the terminal <br>
[Link to more details about the ways to use vue](https://vuejs.org/guide/extras/ways-of-using-vue.html#beyond-the-web)

### 1. 1 Vue's core features <a name="coreFeatures"></a>

1. 11 Virtual DOM <a name="VDOM"></a>
    
     A layer over the real DOM that absorbs the changes, the changes is then compared to with the DOM. Only final changes will be updated to the real DOM. 

     The virtual DOM what Vue's internal rendering mechanism is based upon.

    >The virtual DOM (VDOM) is a programming concept where an ideal, or “virtual”, representation of a UI is kept in memory and synced with the “real” DOM. The concept was pioneered by React, and has been adapted in many other frameworks with different implementations, including Vue. <br>
     > ... 
     >
     >The main benefit of virtual DOM is that it gives the developer the ability to programmatically create, inspect and compose desired UI structures in a declarative way, while leaving the direct DOM manipulation to the renderer.
     >[Qoute from vuejs.org](https://vuejs.org/guide/extras/rendering-mechanism.html#virtual-dom)

<br>


 A div representet, can look like this: 



``` js 

const vnode = {
  type: 'div',
  props: {
    id: 'hello'
  },
  children: [
    /* more vnodes */
  ]
}
```

In a process called mount, a runtime renderer contructs the DOm tree from the virtual DOM. 

**Links to more knowledge about the VDOM**

[How the virtual dom works in vue js](https://blog.logrocket.com/how-the-virtual-dom-works-in-vue-js/) <br>
[Vue's documentation on the rendering mechanism](https://vuejs.org/guide/extras/rendering-mechanism.html) <br>
[Video about vue's virtual DOM](https://www.youtube.com/watch?v=F3TQs1pUgzM) <br>
[Something about what a virtual DOM is](https://www.linkedin.com/learning/mastering-web-developer-interview-code-2/what-is-the-virtual-dom?autoplay=true&resume=false&u=57075649) <br>
[Learn to build on the DOM - Basic js ](https://www.linkedin.com/learning/vanilla-javascript-building-on-the-document-object-model-dom/learn-the-javascript-dom-inside-and-out?autoplay=true&u=57075649)


