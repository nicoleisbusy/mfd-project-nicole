# Strapi
* [Strapi dashboard](#strapi-dashbord)
  * [Content manager](#S-content-manager)
  * [Content type builder](#S-content-type-builder)
    * [Collection types](#S-collection-types)
    * [Single type](#S-single-type)
    * [Components](#S-components)
    * [Content type](#S-content-types)
      * [Simple/static properties](#S-simple/static-properties)
      * [“Relation” properties](#S-rel-properties)
      * [Relation properties](#S-relation-properties)
      * [‘Advanced settings’](#S-advanced-settings)
  * [Media library](#S-media-library)
  * [Documentation](#S-dokumentation)
  * [Marketplace](#S-marketplace)
  * [Settings](#S-settings)
    * [Global settings](#S-global-settings)
    * [Administration panel](#S-administration-panel)
      * [Roles](#S-panel-roles)
      * [Users](#S-panel-users)
    * [Email plugin](#S-email-plugin)
      * [Configuration](#S-configuration)
  * [Users & Permissions plugin](#S-users-and-permissions-pluging)
    * [Roles](#S-roles)
    * [Providers](#S-providers)
    * [Email templates](#S-email-template)
    * [Advanced settings](#S-advanced-settings)
  * [How to Strapi](#how-to)
    * [Create collection types](#HTS-collection-types)
    * [Create single type](#HTS-single-type)
    * [Create component](#HTS-component)
    * [Create entries in the content types](#HTS-entries)
    * [Make content public](#HTS-content-public)
    * [Rest API](#HTS-rest-api)
      * [API endpoints](#HTS-api-endpoints)
      * [Endpoints](#HTS-endpoints)
      * [Unifield response format](#HTS-unifield-respone-format)
    * [Create a dynamic zone](#HTS-dynamic-zone)
  * [Links og dokumentation](#l&d)

Supports NodeJS (version 13-16)  

## Strapi dashboard <a name="strapi-dashbord"></a>

### Content manager <a name="S-content-manager"></a>
Modelize the data structure of your API. Create new fields and relations in just a minute. The files are automatically created and updated in your project  

// Plugins
### Content type builder <a name="S-content-type-builder"></a>
Quick way to see, edit and delete the data in your database.

#### Collection types <a name="S-collection-types"></a>
- Multiple instances
- Collection types are content-types that can manage several entries

#### Single type <a name="S-single-type"></a>
- Data that is not meant to be repeated
- Home page is a single type
- Single instances
  - *“Unlike collection types which have multiple entries, single types are not created for multiple uses. In other words, there can only be one default entry per available single type. There is therefore no list view in the Single types category.”* - Strapi
- Single types are content-types that can only manage one entry.

#### Components <a name="S-components"></a>
- Fields that also will be needed for other content types
- Reusable content
- Components are a data structure that can be used in multiple collection types and single types

#### Content types <a name="S-content-types"></a>

##### Simple/static properties <a name="S-simple/static-properties"></a>
- **Text:** Small or long text like title or description  
- **Email:** Email field with validations format
- **Rich text:** A rich text editor with formatting options
- **Password:** Password field with encryption
- **Number:** Numbers (integer, float, decimal)
- **Enumeration:** List of values, then pick one
- **Date:** A date picker with hours, minutes and seconds
- **Boolean:** Yes or no, 1 or 0, true or false
- **JSON:** Data in JSON format
- **UID:** Unique identifier

##### “Relation” properties <a name="S-rel-properties"></a>
- **Media:** Files like images, videos, etc
Creates a relation with a file object  
- **Relation:** Refers to a Collection Type
Refers to other content types

##### Relation properties <a name="S-relation-properties"></a>
- **Component:** Group of fields that you can repeat or reuse
Allow group fields to repeat across various content types
Groups of added fields that are meant to be reused in (many) different content types
- **Dynamic zone:** Dynamically pick component when editing content
Allow content editors to choose among different components for each single content item (like ‘Widgets’ in WordPress)
Can be used to: ‘Similar product’/’You might also like’
Tags, categories and … in common

##### ‘Advanced settings’ <a name="S-advanced-settings"></a>
- Required field: You won't be able to create an entry if this field is empty
- Unique field: You won't be able to create an entry if there is an existing entry with identical content
- Maximum length
- Minimum length
- Private field: This field will not show up in the API response

### Media library <a name="S-media-library"></a>
Media file management

### Documentation <a name="S-dokumentation"></a>

// General
### Plugins <a name="S-plugins"></a>
Create an OpenAPI Document and visualize your API with SWAGGER UI
- List of the installed plugins in the project

### Marketplace <a name="S-marketplace"></a>

### Settings <a name="S-settings"></a>

#### Global settings <a name="S-global-settings"></a>

#### Administration panel <a name="S-administration-panel"></a>

##### Roles <a name="S-panel-roles"></a>
- List of roles
  - Author: Can manage the content they have created
  - Editor: Can manage and publish contents including those of other users
  - Super admin: Can access and manage all features and settings
- Up to three admin roles
- Controls what users can do in the Strapi admin panel


##### Users <a name="S-panel-users"></a>
All the users who have access to the Strapi admin panel

#### Email plugin <a name="S-email-plugin"></a>

##### Configuration <a name="S-configuration"></a>

#### Users & Permissions plugin <a name="S-users-and-permissions-pluging"></a>

##### Roles <a name="S-roles"></a>
- Content is by default not public and therefore not accessible for others than admin users
- Data is available via the API

##### Providers <a name="S-providers"></a>

##### Email templates <a name="S-email-template"></a>

##### Advanced settings <a name="S-advanced-settings"></a>

## How to Strapi <a name="how-to"></a>

### Create collection types <a name="HTS-collection-types"></a>
1. ‘Create new collection type’
2. Enter an understandable Display Name (singular)
3. Select a field
4. Enter an understandable name for the field
5. ‘Finish’
6. ‘Save’

### Create single type <a name="HTS-single-types"></a>
1. ‘Create new single type’
2. Enter an understandable Display Name (singular)
3. Select a field
4. Enter an understandable name for the field
5. ‘Finish’
6. ‘Save’

### Create component <a name="HTS-component"></a>
1. ‘Create new component’
2. Enter an understandable Display Name
3. Select a category or make a new one
4. Select a icon for the component
5. Select a field (or more) for your component
6. ‘Save’
7. Select the chosen collection type
8. ‘Add another field’
9. Select ‘Component’
10. ‘Use an existing component’
11. Enter an understandable Name
12. Select the chosen component in ‘Select a component’
13. Select the component type
  * Repeatable component: Multiple instances
  * Single component: Grouping fields
16. ‘Save’

### Create entries in the content types <a name="HTS-entries"></a>
1. Select the collection type
2. ‘Create new entry’
3. Type in the content for the field/fields
4. ‘Save’
5. ‘User’ are registered users on the site/in the application

### Make content public <a name="HTS-content-public"></a>
1. Go to ‘Settings’
2. ‘Users & Permissions plugin’ → ‘Roles’
3. ‘Public’ → find the content type and click on the arrow down
4. Select ‘find’ and ‘findOne’
5. ‘Save’

### Rest API <a name="HTS-rest-api"></a>
- The REST API allows accessing the content-types through API endpoints that Strapi automatically creates.
- API parameters can be used to filter, sort, and paginate results and to select fields and relations to populate). Additionally, specific parameters related to optional Strapi features can be used, like publication state and locale.

| Operator | Type | Description |
| ---------| -----|------------ |
| sort | String/Array | Sorting the response |
| filters | Object | Filter the response |
| populate | String/Object | Populate relations, components, or dynamic zones |
| fields | Array | Select only specific fields to display |
| pagination | Object | Page through entries |
| publicationState | String | Select the draft & publish state <br> Only accepts the following values: <br> - Live <br> - Preview|
| locale | String/Array | Select one ore multiple locales |

#### API endpoints <a name="HTS-api-endpoints"></a>
- Creating a content-type automatically creates some REST API endpoints available to interact with it.

##### Endpoints <a name="HTS-endpoints"></a>
- For each Content-Type, the following endpoints are automatically generated:

| Method | URL | Description |
| ---------| -----|------------ |
| GET | /api/:pluralApiId | Get a list of entries |
| POST | /api/:pluralApiId | Create an entry |
| GET | /api/:pluralApiId/:documentId | Get an entry |
| PUT | /api/:pluralApiId/:documentId | Update an entry |
| DELETE | /api/:pluralApiId/:documentId | Delete an entry |

##### Unified response format <a name="HTS-unifield-respone-format"></a>
- Whatever the query, the response is always an object with the following keys:
  - data: the response data itself, which could be:
    - a single entry, as an object with the following keys:
      - id (number)
      - attributes (object)
      - meta (object)
    - a list of entries, as an array of objects
    - a custom response
  - meta(object): information about pagination, publication state, available locales, etc.
  - error (object, optional): information about any error thrown by the request

### Create a dynamic zone <a name="HTS-dynamic-zone"></a>
1. Chose the chosen type → ‘Add another field to this … type’
2. ‘Dynamic zone’
3. Enter an understandable name (singular)
4. ‘Add components to the zone’
5. ‘Use an existing component’
6. Select the components
7. ‘Finish’
8. ‘Save’

## Links og dokumentation <a name="l&d"></a>
 [Strapi v4: complete crash course (2022)](​​https://youtu.be/HjhK0pzwlbU)
