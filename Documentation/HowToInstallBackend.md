# How to install Our backend 

## 1. Install heroku on your pc. 

### For mac: 

[Download for mac](https://cli-assets.heroku.com/heroku.pkg)

Via Homebrew: 

```
brew tap heroku/brew && brew install heroku
```

### For windows:

[Download for windows 64-bit]( https://cli-assets.heroku.com/heroku-x64.exe) 
[Download for windows 32-bit](https://cli-assets.heroku.com/heroku-x86.exe) 


### Ubuntu: 

```
sudo snap install --classic heroku
```

## 2. Clone the project  

Clone the project into a new folder, which is not inside a git folder. 

```
 git clone git git@gitlab.com:t7353/sebackend.git
```

Then you will see something like this

```
Emil@DESKTOP-0JSK7FR MINGW64 /d/Katharina/webudvikling/Projekt2/Gitlab
$ git clone git@gitlab.com:t7353/sebackend.git
Cloning into 'sebackend'...
Enter passphrase for key '/c/Users/Emil/.ssh/id_ed25519':
remote: Enumerating objects: 78, done.
remote: Counting objects: 100% (78/78), done.
remote: Compressing objects: 100% (56/56), done.
remote: Total 78 (delta 12), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (78/78), 27.61 KiB | 2.76 MiB/s, done.
Resolving deltas: 100% (12/12), done.

```

## 3. Installing packages

After that you can install your NPM packages 

```
npm install
```

Then you would see something like this 

```
npm WARN deprecated source-map-url@0.4.1: See https://github.com/lydell/source-map-url#deprecated
npm WARN deprecated urix@0.1.0: Please see https://github.com/lydell/urix#deprecated
npm WARN deprecated har-validator@5.1.5: this library is no longer supported
npm WARN deprecated mailcomposer@3.12.0: This project is unmaintained
npm WARN deprecated source-map-resolve@0.5.3: See https://github.com/lydell/source-map-resolve#deprecated
npm WARN deprecated resolve-url@0.2.1: https://github.com/lydell/resolve-url#deprecated
npm WARN deprecated debug@4.1.1: Debug versions >=3.2.0 <3.2.7 || >=4 <4.3.1 have a low-severity ReDos regression when used in a Node.js environment. It is recommended you upgrade to 3.2.7 or 4.3.1. (https://github.com/visionmedia/debug/issues/797)
npm WARN deprecated formidable@1.2.6: Please upgrade to latest, formidable@v2 or formidable@v3! Check these notes: https://bit.ly/2ZEqIau
npm WARN deprecated buildmail@3.10.0: This project is unmaintained
npm WARN deprecated querystring@0.2.0: The querystring API is considered Legacy. new code should use the URLSearchParams API instead.
npm WARN deprecated uuid@3.4.0: Please upgrade  to version 7 or higher.  Older versions may use Math.random() in certain circumstances, which is known to be problematic.  See https://v8.dev/blog/math-random for details.
npm WARN deprecated request@2.88.2: request has been deprecated, see https://github.com/request/request/issues/3142
npm WARN deprecated node-pre-gyp@0.11.0: Please upgrade to @mapbox/node-pre-gyp: the non-scoped node-pre-gyp package is deprecated and only the @mapbox scoped package will recieve updates in the future
npm WARN deprecated tar@2.2.2: This version of tar is no longer supported, and will not receive security updates. Please upgrade asap.
npm WARN deprecated sequelize@5.22.5: Please update to v6 or higher! A migration guide can be found here: https://sequelize.org/v6/manual/upgrade-to-v6.html

added 1591 packages, and audited 1592 packages in 2m

140 packages are looking for funding
  run `npm fund` for details

12 vulnerabilities (9 moderate, 3 high)

To address issues that do not require attention, run:
  npm audit fix

To address all issues possible (including breaking changes), run:
  npm audit fix --force

Some issues need review, and may require choosing
a different dependency.

Run `npm audit` for details.

```

## 3.  Add Heroku as a remote 

When where pulling from this repository, it's not set up with the remote acces to heroku, this i how you can set it up: 

```
git remote add heroku https://git.heroku.com/still-scrubland-97284.git
```

## 4. Checking if all is okay

To se what remotes that is avaiable now try running: 

``` 
git remote
```

Then you should see something like this: 

```
heroku
origin
```

To check if you have acces to heroku online, you have to login, make sure you have heroku installed 

```
heroku login
```

You should see something like this: 


```
 Warning: heroku update available from 7.53.0 to 7.59.3.
heroku: Press any key to open up the browser to login or q to exit: 

```


if **you are using git bash** you would have to type something and then press enter. 

it could look  something like this, and it will work 

```
 »   Warning: heroku update available from 7.53.0 to 7.59.3.
heroku: Press any key to open up the browser to login or q to exit: ggf
Opening browser to https://cli-auth.heroku.com/auth/cli/browser/ebd58afa-af68-4601-a777-23f940306c98?requestor=SFMyNTY.g2gDbQAAAA42Mi4xMDcuMjM0LjEwMG4GABifgll_AWIAAVGA.tXNRIGmxoEwMAmyRc_Qs6KwuJ09Kud6FVvrbhLgEcao
heroku: Waiting for login...


```

If **if it doesn't work, try another terminal**



When it works, you can now login. 

Use login and password found in logbog on day 05 marts , 

after you are logged in, you will see something like this: 

```
Logging in... done
Logged in as katharinaappel@gmail.com


```


you can now switch to at new git bash and type 

```
heroku open
```

it will transport you to a site without a button, but just type /admin after the adress

or go to 

[StapiAdminLink](https://still-scrubland-97284.herokuapp.com/admin)

You will have to login. 

Use login and password found in logbog on day 05 marts 

After that you'll been taken to the dashboard page. 

### Checking local 

After that you'll want to make sure that everything is ok with you're local development enviroment. 

```
npm run develop
```

Here you'll have to create your own login for your own local development, for every pc.  This will not get pushed to the online serve. 

Look around a see if everything looks like it should. 

Make sure you go to this page and check if it's running. 

[Api Documentation Local](http://localhost:1337/documentation/v1.0.0)

After that you'll be ready. 

