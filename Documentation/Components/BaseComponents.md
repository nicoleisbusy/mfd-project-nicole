# Base components 

 1.  [Text Components](#TextComponents) <br>
    1. 1  [Base H1 Heading](#BaseH1Heading) <br>
    1. 2  [Base H2 Heading](#BaseH2Heading) <br>
    1. 3  [Base H3 Heading](#BaseH3Heading) <br>
    1. 4  [Base H4 Heading](#BaseH4Heading) <br>
    1. 5  [Base H5 Heading](#BaseH5Heading) <br>
    1. 6  [Base H6 Heading](#BaseH6Heading) <br>
    1. 7  [Base External Link](#BaseExternalLink) <br>
    1. 7  [Base Email Link](#BaseEmailLink) <br>
 1.  [Graphic components](#GraphicComponents) <br>
    2. 1  [BaseSoMeLink](#BaseSoMeLink) <br>
    2. 2  [](#) <br>
 1.  [](#) <br>
 1.  [](#) <br>
 1.  [](#) <br>
 1.  [](#) <br>
 1.  [](#) <br>

 ## 1. Text Components <a name="TextComponents"></a>

  ### 1 . 1   Base H1 Heading <a name="BaseH1Heading"></a>
  
  ``` vue
  
  <template>
 <BaseH1 theme="nameoftheme"  heading1 = "The text"/>
</template>

<script>
import BaseH1 from "@/components/base/BaseBlockH2.vue";

export default {
components: { BaseH1}
}
</script>

<style>

</style>

  ```
### Classes

- base-h1
- variable class: theme 


### Props  
    

 - **heading1** are for all the text. 

    - String  

 - **theme** 
  
    - "Dark", is a class added for black text   

 ### 1 . 2  Base H2 Heading <a name="BaseH2Heading"></a>

 ``` vue
<template>
 <BaseH2 text="New " heading2 = "video out now"/>
</template>

<script>
import BaseH2 from "@/components/base/BaseH2.vue";

export default {
components: { BaseH2}
}
</script>

<style>

</style>


 ```

This heading is a little special. 
It has the ability to make the first word yellow. 

   ```css

   
h2:before{
	content:attr(data-text);
	color: $secondary-font-color;
	
}
   
   ```
 ### Classes

- base-h2
- variable class: theme 

### Props  

-  **text** is where we can write the first letter in yellow if we want. 
    
     - Any string you need     

 - **heading2** are for all the text. 

    - String  

 - **theme** 
 
    - "Dark", is a class added for black text   

  ### 1 . 3 Base H3 Heading <a name="BaseH3Heading"></a>

 ``` vue
<template>
 <BaseH3  heading3 = "video out now"/>
</template>

<script>
import BaseH3 from "@/components/base/BaseH3.vue";

export default {
components: { BaseH3}
}
</script>

<style>

</style>


 ```
 ### Classes

- base-h3
- variable class: theme 

### Props      

 - **heading3** are for all the text. 

    - String  

 - **theme** 
 
    - "Dark", is a class added for black text   

  ### 1.4 Base H4 Heading <a name="BaseH4Heading"></a>
   ``` vue
<template>
 <BaseH4 text="New " heading4 = "video out now"/>
</template>

<script>
import BaseH4 from "@/components/base/BaseH4.vue";

export default {
components: { BaseH4}
}
</script>

<style>

</style>


 ```
 ### Classes

- base-h4
- variable class: theme 

### Props     

 - **heading4** are for all the text. 

    - String  

 - **theme** 
 
    - "Dark", is a class added for black text   


  ### 1.5 Base H5 Heading <a name="BaseH5Heading"></a>
   ``` vue
<template>
 <BaseH5 heading5 = "video out now"/>
</template>

<script>
import BaseH5 from "@/components/base/BaseH5.vue";

export default {
components: { BaseH5}
}
</script>

<style>

</style>


 ```
 ### Classes

- base-h5
- variable class: theme 

### Props  
   

 - **heading5** are for all the text. 

    - String  

 - **theme** 
 
    - "Dark", is a class added for black text   

  ### 1.6 Base H6 Heading <a name="BaseH6Heading"></a>

   ``` vue
<template>
 <BaseH5 heading6 = "video out now"/>
</template>

<script>
import BaseH6 from "@/components/base/BaseH6.vue";

export default {
components: { BaseH6}
}
</script>

<style>

</style>


 ```
 ### Classes

- base-h6
- variable class: theme 

### Props  
   

 - **heading6** are for all the text. 

    - String  

 - **theme** 
 
    - "Dark", is a class added for black text 

<br>

  ### 1.7  Base External Link <a name="BaseExternalLink"></a>

  It has **class="base-external-link"**

  It takes a prop with the name link

  And will take a text in it's slot. 

   To implement the component use: 
 
 ``` vue
<template>
 <BaseExternalLink link="your link">
   Your link text here
 </BaseExternalLink>
</template>

<script>
import BaseExternalLink from "@/components/base/BaseExternalLink.vue";

export default {
components: { BaseExternalLink}
}
</script>

<style>

</style>


 ```

 ### Classes
    
- base-external-link


### Props  
   
- link

- slot 
   
   - this will take any text or component in it's slot 

 


 ### 1.8  Base Email Link <a name="BaseEmailLink"></a>

 ``` vue 
 <template>
    <BaseEmailLink emailadress="mailto: spectrumemission@gmail.com" linktext="SpectrumEmission@gmail.com"/>

</template>

<script>
import BaseEmailLink from "@/components/base/BaseEmailLink.vue";
export default {
  components: { BaseEmailLink },
};
</script>

<style>
</style>


 ```

### Classes

- base-email-link



### Props  

- linktext 

   - String, the email, or a call to action 

- emailadress 
   
   - mailto: + email adress
   


 ## 2. Graphic Components <a name="GraphicComponents"></a>

### 2 . 1 Base SoMe Link <a name="BaseSoMeLink"></a>

**It's takes one prop: link**

It has **class="base-some-link"**

>**fa** is a fontawesome component, the component must be introduced as below in a slot,
     or else it doesn't work properly 
>
>Learn more about [ font awesome and vue 3 here](https://www.youtube.com/watch?v=MoDIpTuRWfM)



``` vue
<template>
   
<BaseSoMeLink link="www.youtube.com/channel/UCKPd6L2RfyZzO1Wk8PiKQlQ">
<fa :icon="['fab', 'youtube']" /> 
</template>

<script>
import BaseSoMeLink from "@/components/base/BaseSoMeLink.vue";
export default {
  components: { BaseSoMeLink },
};
</script>

<style>
</style>
```