# Branching 

1. [Useful commands with some explanation](#Common) <br>
     1.1  [List branches](#list-branches) <br>
    1.2 [Make a branch, without checking it out](#mk-branch) <br>
    1.3 [Safe delete branch locally](#safe-d) <br>
    1.4 [Force delete branch locally](#force-d) <br>
    1.5 [How to go to a branch](#go-branche) <br>
    1.6 [Make and check out at the same time](#make-check) <br>
    1.7 [Renaming of a branch](#renaming) <br>
    1.8 [Deleting a remote branch](#d-remote) <br>
    1.81 [What Happens If I Delete a Git Branch](#d-branch) <br>
    1.82 [Deleting a Branch With Merged Changes](#d-with-merge)<br>
    1.83 [Deleting a Branch With Unmerged Changes](#d-un-merged)<br>
    1.9 [Prune branches (delete local branches)](#prune) <br>
    1.10 [Push branch upstream](#push-branch) <br>
    1.11 [List all local and remote branches](#list-all) <br>
    1.12 [List all remote branches](#list-remote) <br>
2.  [Creating a new Branch Tutorial](#create-tutorial) <br>
3. [Tutorials for merging branches](#merge-tut)
    [Good videos](#good-video) <br>
    [Links](#links)
## 1. Usefull commands  <a name="Common"></a>

###     1.1 List branches <a name="list-branches"></a>

```
git branch
```
 
```
git branch --list 
```

List all of the branches in your repository. <br> 

git branch --list, works in gitbash og windows cmd, but not in vs code terminal <br>

<br>

###     1.2 Make a branch without checking it out <a name="mk-branch"></a>

```
git branch yourbranchname 
```

###     1.3 safe delete branch locally  <a name="safe-d"></a>

```
git branch -d yourbranchnameyouwanttodeletesafely 
```
Deletes only branches with merged changes 

>[From Cloudbee's blog ]()
>After running the command, Git will let you know the branch was successfully deleted. It will also display the hash of the commit pointed to by the branch:
>
> Fx: Deleted branch new (was c9bd965)

###     1.4  Force delete branch locally <a name="force-d"></a>

```
git branch -D yourbranchnameyouwanttodeleteFORCEFULLY 
```
Deletes the named branch, even with unmerged changes. <br>
Commonly used when you want to permanently throw away all of the commits associated with a particular line of development.

###     1.5 How to go to the new branch <a name="go-branche"></a>

This action is called checking a branch out. 

```
git checkout thenameofyourbranch
```
The git checkout command operates upon three distinct entities: files, commits, and branches. <br>
The git checkout command lets you navigate between the branches created by git branch.

Checking out a branch updates the files in the working directory to match the version stored in that branch, and it tells Git to record all new commits on that branch.

###     1.6  How to make a new local branch a check it out simultaneously <a name="make-check">  </a>

```
git checkout -b nameyournewbranch
```


###     1.7 Re-naming a local branch <a name="renaming"></a>

When you inside your branch it look like this in your gitbash, notice the cyan text saying testing <br> 

![insideabranch](/Documentation/DocumentationGraffic/insidebranch.png) <br> 

This will not show up in the vs code terminal or cmd. 

While inside, write this command to rename your branch
```
git branch -m thenewnameforyourbranch 
```

###     1.8 Deleting a remote branch <a name="d-remote"></a>

```
git push origin --delete nameofyourremotebranch  
```
> Quotes from [Cloud Bees blog](https://www.cloudbees.com/blog/git-delete-branch-how-to-for-both-local-and-remote) <br>
>You don’t use the ```git branch``` command to delete a remote branch. You use ```git push```, even if that sounds weird.
It’s like you’re pushing—sending—the order to delete the branch to the remote repository.
> 
> ### 1.81 What Happens If I Delete a Git Branch? <a name="d-branch"></a>
>When you delete a branch in Git, you don’t delete the commits themselves. That’s right: The commits are still there, and you might be able to recover them.
>
>
>
> ### 1.82 Deleting a Branch With Merged Changes <a name="d-with-merge">
> ...if you delete the remote branch, nothing changes. The commits were already integrated into the original branch
> ### 1.83 Deleting a Branch With Unmerged Changes <a name="d-un-merged">
>Now let’s explore the scenario where the branch contains unmerged changes. Take a look at the diagram the depicts the moment after you’ve added the new commits to exp but haven’t yet merged them back to main:
>
> ![Remote](/Documentation/DocumentationGraffic/delete-branch.webp)
>
>If you go now and get rid of the exp branch, your repo will look like this:
>![Picture of af remote branch](/Documentation/DocumentationGraffic/remote-gone.webp)
>As you can see, the commits are still there. But unless you have the hash of the last commit, you won’t be able to reach them, why? 
>
>A commit has a reference to its parent—or parents—but not to its children. From the commit that main points to, you can only move backward, so there’s no way for you to reach the commits formerly pointed to by exp.
>
>As you’ve seen, when you delete a branch, Git displays the hash of the commit the branch pointed to. If you write that down, you can later use it to access that commit and create a new branch from that point:
> ``` 
>git checkout thehashyouwrote down
>git branch yourbranchename
>```
>What if you didn’t write it down? There’s still hope for you in the name of a command called reflog.
>
>In a nutshell, git reflog allows you to see the reference logs. At its most basic form, you can use it to see the list of commits that the HEAD reference pointed to over time.
>
>As an example, let’s delete a branch locally:
>```
>git branch -D new
>```
>
>
>Now, suppose I made a terrible mistake and the branch wasn’t supposed to be deleted. I can use the ```git reflog``` command:
>![Picture of a reflog ](/Documentation/DocumentationGraffic/reflog.webp)
>
>Having recovered the commit hash, I can then do (remember your actual hash number will be different):
>```
>git checkout 984a656
>
>git branch nameyourbranch
>```
>Finally, a caveat: git reflog will only save you if the HEAD reference has pointed to the commit you want to recover. You might have scenarios where the desired commit isn’t actually in the reflog, so there will be no way for you to recover its hash.

###     1.9 Prune branches (delete local branches) <a name="prune">

``` 
git fetch -p 
```
The -p flag means "prune". After fetching, branches which no longer exist on the remote will be deleted.

>A qoute from this [stackoverflow](https://stackoverflow.com/questions/17832850/what-does-git-fetch-p-or-prune-means) about pruning, and why we could need it. 
>
>When you fetch a remote repository, say “origin”, you will get remote branches for each branch that exists on that remote repository. Those branches are locally stored as  ```<remote>/<branch>.```
>
>So assume origin has branches master, featureX and featureY. Then after fetching the following “remote branches” exist in your local repository: origin/master, origin/featureX and origin/featureY.
>
>Now, imagine someone else merges featureX into master and removes the feature branch from the remote repository. Then origin only has two branches master and featureY.
>
>However, when you fetch, the three remote branches will all still exist, including the one that was deleted in the remote repository. This is to prevent you from accidentally losing the branch (imagine someone accidentally removed the branch from the remote, then everyone who fetched from it would also lose it, making it hard to restore it).
>
>Instead, you will need to tell the fetch command to prune any branches that no longer exist on the remote branch. So by executing ```git fetch --prune origin``` or ```git fetch -p``` the remote branch origin/featureX will be removed too.

### 1.10 Push branch upstream <a name="push-branch"> </a>

```
git push --set-upstream origin nameofyourbranch 
```

### 1.11  List all local and remote branches <a name="list-all"></a>

```
git branch -a 
```

### 1.12  List all remote branches <a name="list-remote"></a>

```
git branch -r 
```

#     2.0 Creating a new Branch Tutorial <a name="create-tutorial"></a>

Before creating a new branch, pull the changes from upstream. Your master needs to be up to date.

```
git pull
```

To create a branch write following. 

```
git branch yourbranchname 
```

To check if your branch is there, write following. 

```
git branch
```
or
 
```
git branch --list 
```
This will make a list of the branches available, like so: <br>
>![ ](/Documentation/DocumentationGraffic/branch-making.png) 

To  move to the branch, you can checkout the branch name with this command: 

```
git checkout thenameofyourbranch
```
It'll look something like this:  <br>
>![ ](/Documentation/DocumentationGraffic/checkout.png)

If you want to create and checkout a branch a the same time use: 
```
git checkout -b nameyournewbranch
``` 

the ```-b ``` is for branch. 

Then it would look something like this: <br>
![ ](/Documentation/DocumentationGraffic/checkout-new-branch.png)

But the thing is, these branches are just local branches. 
To get the branch visible on gitlab, you must push the branch up stream like this. 

```
git push --set-upstream origin nameofyourbranch 
```
It'll look something like this: 

![ ](/Documentation/DocumentationGraffic/upstream.png)

And now, the branchs is avaiable on gitlab. <br
>
![ ](/Documentation/DocumentationGraffic/available.png)




[Link to a Video Tutorial about this](https://youtu.be/jXHdlvor0nY)
 

<br>

# 3.0 Tutorials for merging branches <a name="merge-tut">

After you have created af branch and worked on it for at bit and want to merge it with the main branch, there's several senarios that can happen. 

## 3.1 Senario one, you are working on an branch that is a head of main.

After you have been working for a while doing some commits on your branch, you'll be a head of main, if no one else has pushed their commits. 

You'll have to checkout to main, and then merge the wanted branch like so 

```
$ git checkout main
$ git merge nameofthebranchyouwanttomerge
```

Then you'll get at message looking something like this: 

```
Updating 0637608..6b99d3c
Fast-forward
 Documentation/Branches.md                   | 4 ++++
 Documentation/DocumentationGraffic/test.txt | 1 +
 2 files changed, 5 insertions(+)
```
> [From git-scm.com](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging)
>You’ll notice the phrase “fast-forward” in that merge. Because the commit C4 pointed to by the branch hotfix you merged in was directly ahead of the commit C2 you’re on, Git simply moves the pointer forward. To phrase that another way, when you try to merge one commit with a commit that can be reached by following the first commit’s history, Git simplifies things by moving the pointer forward because there is no divergent work to merge together — this is called a “fast-forward.”

When your finished with your branch, you would like to delete it. 


for local 

```
git branch -d  nameofyourbranch  
```

for remote

```
git push origin --delete nameofyourremotebranch  
```

##  3.2 Senario two, when main is a head of you, but no conflicts 

After working for a while, a some point main will be ahead of the branches common ancester. The first steps are the same as in senario 2, but the msg wil be difirrent. 


```
$ git checkout main
$ git merge nameofthebranchyouwanttomerge
```

Then you'll get at message looking something like this: 

```
Merge made by the 'recursive' strategy.
index.html |    1 +
1 file changed, 1 insertion(+)
```

> [From git-scm.com](https://git-scm.com/book/en/v2/)
> In this case, your development history has diverged from some older point. Because the commit on the branch you’re on isn’t a direct ancestor of the branch you’re merging in, Git has to do some work. In this case, Git does a simple three-way merge, using the two snapshots pointed to by the branch tips and the common ancestor of the two.
>
>Instead of just moving the branch pointer forward, Git creates a new snapshot that results from this three-way merge and automatically creates a new commit that points to it. This is referred to as a merge commit, and is special in that it has more than one parent.


When your finished with your branch, you would like to delete it. 


for local 

```
git branch -d  nameofyourbranch  
```

for remote

```
git push origin --delete nameofyourremotebranch  
```



### Good videos  <a name="good-videos"> </a>
[Git for GitLab (Beginner's FULL COURSE)](https://www.youtube.com/watch?v=4lxvVj7wlZw) <br>
Brancing starts at 31:01. 

### Links to more information <a name="links"> </a>


[Git branching remote branches](https://git-scm.com/book/en/v2/Git-Branching-Remote-Branches) <br>
Learn more about remote branches 


https://www.atlassian.com/git/tutorials/syncing 


https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging 

https://git-scm.com/docs 

