# Heading 1
## Heading 2
### Heading 3

[https://www.ucl.dk](https://www.ucl.dk)

![IMG](img.jpg)

| Kolonne 1| Kolonne 2 | Kolonne 3 | Kolonne 4 |
| :- | :-: | :-: | -: |
| Venstre | Centeret | Centeret | Højre |
| Venstre | Centeret | Centeret | Højre |
| Venstre | Centeret | Centeret | Højre |

- En
  - To
    - Tre

1. Første
  2. Anden

**Fed**
*Italic*
***FedItalic***

>`Markdown`
>
