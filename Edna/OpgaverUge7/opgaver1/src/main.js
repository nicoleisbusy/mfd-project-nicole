//this is the DOM file

import { createApp } from 'vue' 
import App from './App.vue' // importing a vue components
import './assets/global.css'


createApp(App).mount('#app')
