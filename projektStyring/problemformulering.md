# Problemformulering
Mål: skal være løsningsfokuseret med udgangspunkt i projektets krav.
[Læringsmål (fra studieordningen)](/krav-og-laeringmaal.md)

## Nuværrende version

### Version 3
**Emne:**
Band hjemmeside og webshop

**Problemområde:** 
En hjemmeside med webshop som kan fungere som et online samlepunkt.
- Hjemmeside med webshop
- Online samlepunkt for deres online presence
- Integration med API fra andre sociale medier
- Vue og webshop
- Kvalitetssikring og vedligeholdelse.

**Problemformulering:**
Hvordan kan en band hjemmeside med webshop udvikles til at være et samlingspunkt for bandets online presence?

- Hvordan kan API integration understøtte dette samlingspunkt?
- Hvordan kan et framework som Vue 3 bruges til at udvikle en webshop effektivt?
- Hvordan kan hjemmesiden kvalitetsikres, ift videregivelse og vedligeholdelse?

## Tidligere version

### Version 1

Hvordan kan mindre bands samle deres online presence i en løsning?

- Hvad er vigtigst for en fan af et band?
- Hvordan tilgængeliggøres teknologiske redskaber for bands?

### Version 2

Emne: Band hjemmeside og webshop

Problemområde: Hvordan kan man lave en hjemmeside med webshop som kan fungere som et online 
Hvordan kan et band samle deres online presence i en løsning?

- Hvordan tilgængeliggøres teknologiske redskaber for bandet?

