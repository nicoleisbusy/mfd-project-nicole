# Krav og læringsmål

## Læringsmål

### Viden
- Client-side webapplikationsarkitektur
- Style processing og naming conventions
- Frontend eco system
- Frontend workflows
- Frontend frameworks
- Kvalitetssikring

### Færdigheder
- Planlægge, udvikle og implementere/idriftsætte client-side web applikationer baseret på konkrete udviklingsønsker (skal hostes live/deploy, se krav til projekt)
- Vælge egnede værktøjer og metoder til implementering af client-side web applikationer
- Implementere cross-platform web brugergrænseflader
- Implemetere client-side logik
- Anvende metoder til kvalitetssikring af client-side web applikationer

### Kompetencer
- Analysere komplekse udviklingsønsker til at vælge og anvende egnede workflows, metoder, værktøjer og frameworks til implementering af cross-platform, performant, vedligeholdelsesvenlig og dynamiske client-side web applikationer

## Krav til produkt for prøven

### Overordnede krav
De studerende udarbejder i grupper en webapplikation og en rapport på minimum 10 og maksimum 15 normalsider for en gruppe.  
For at kunne vurdere de studerendes individuelle præstation skal det fremgå af indholdsfortegnelsen for rapporten hvilke afsnit, der er udarbejdet af den enkelte studerende.

Webapplikation og kode, som er udviklet af de studerende, skal være tilgængeligt online for censor og eksaminator.

### Teknologi krav
- [ ] Styling skrives i SASS/SCSS med brug af partials.
- [ ] Semantic HTML skal anvendes (se ARIA, accessible rich internet application)
- [ ] Vue3.js og Vue CLI skal anvendes
- [ ] Applikationen skal være en Single Page Application (SPA)
- [ ] Routing skal implementeres med vue router
- [ ] Applikationen skal kvalitets sikres i et offentligt tilgængeligt repository (public github/gitlab etc. projekt)
- [ ] Applikationens kode skal versionsstyres ved brug af GIT
- [ ] Applikationen skal implementere software test
- [ ] Applikationen må meget gerne implementeres som en PWA men det er valgfrit

### Funktionalitets krav
- [ ] Appplikationen skal hente og parse data i JSON format fra en API. API data er applikationens indhold. Det vil være en fordel at bruge et CMS som backend der leverer API data.
- [ ] Applikationen skal implementer dynamisk routing med en eller flere overbliks sider der dynamisk linker til undersider. Dette kaldes også Source-Detail Page.
- [ ] Filtrering skal implementers som f.eks en søge funktion på siden.
- [ ] Applikationen skal have en eller flere input forms og der skal som minimum implementeres validering in front-end, men gerne suppleres med back-end validering.
- [ ] Applikationen skal bygges responsivt, mobile first, men fungere cross platform på relevante platform.
- [ ] Applikationen skal udgives så den er offentligt tilgængelig for brugere. Appliaktionen skal køre fra aflevering af rapport til 1 uge efter alle eksamens forsøg.

### Rapport krav

#### Forside
På rapportens forside skal der specificeres følgende: titel, antal tegn, dato for aflevering,institutionens navn, de enkeltes studerendes navne (udelad CPR), produktets webadresse (URL), Gitlab/Github adresse, samt login informationer til adgang og brug af produktet (brugernavn, password osv.).
Abstract (skal være engelsk)

Projektet skal indeholde et kort abstract. Abstract er ikke det samme som konklusionen. Abstract skal indeholde en kortfattet beskrivelse af, hvad projektets emne er, de vigtigste spørgsmål der er dækket, udforskede områder og konklusioner.
Problemformulering

Projektet skal indeholde en kort, klar og veldefineret problemformulering, der beskriver projektets overordnede problemstillinger og de sonderende spørgsmål, som projektet forsøger at besvare.
Værktøjs afsnit

Rapporten skal indeholde et værktøjsafsnit, hvor der beskrives værktøjer, teknologier og værktøjsmodeller anvendt i projektet.

#### Analyse/Udvikling
Forklar dine valg, teknisk arkitektur og udviklingsprocessen.

#### Diskussion
I denne del af projektet skal du diskutere dine fund / resultater.

#### Konklusion
Det er her i opsummerer hele projektets resultater, så de er helt klare og tydelig for læseren. Her skal i også besvare problemformuleringen.

#### Reflektion
På baggrund af jeres konklusion reflektérer i over, hvad har du lært, hvad ville du gøre anderledes, fremtidige planer osv.