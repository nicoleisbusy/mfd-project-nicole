# Risiko Vurdering 

> ## 1. Sygdom <br>
> Risko: 5 Impact: 2  <br>
><br>
> Ved kortere sygdom: <br>
> Opdater gruppen om sygdom, lav forventlings afstemning omkring produktivitets niveau. <br>
>Ved længere sygdom orienteres underviseren ligeledes. 
 <br>
 
 
> ## 2. Sygdom blandt undervisere <br>
> Risko: 4 Impact: 3 <br>
><br>
> Vi samles og læser videre selv 

> ## 3. At vi ikke når vores milepælene
> Risko: 2 Impact: 4 <br>
><br>
> Vi holder godt øje med gitlab, vi definerer milepæle skarpt. <br>
> Efter hver milepæl er der et milepæl evaluering. <br>
> Hvordan kan vi blive bedre? Er vi til tiden? 
> HVordan ser vores tidsplan nu ud? 

> ## 4. Interne konflikter 
> Risko: 1 Impact: 3 <br>
><br>
> Overholde aftaler og deadlines. 
> Sætte grænser og sige til hvis der er noget man syntes er galt.
> Sige til hvis opgaven er for stor eller for lille 
> Hvis der er relationelle problemer, går vi til projektleder <br>



> ## 5. Ikke overholder aftaler 
> Risko: 2 Impact: 3 <br>
><br>
> Første gang: Vi snakker <br>
> Anden gang: Advarsel <br>
> Tredje gang: Vi tager fat i en underviser <br>
> Hvis det fortsætter kan udvisning komme på tale.

> ## 6. Faglige udfordring ift. teknologi
> Risko: 5 Impact: 1 <br>
> <br>
> Det er vores ansvar at undersøge teknologier, holde os nysgerrig og undersøge 
> når der opstår problemer unden at give op. <br>
><br> 
>Mens vi arbejder sammen fysisk skal man spørge om hjælp efter 10 min.
selvstændigt efter en time.


>## 7. Manglende resourser
> Risko: 2 Impact: 1 <br>
><br>
> Vi nå tænke kreativt og prøve vores bedste på at finde en anden løsning. <br>
> Vi spørger vores netværk

> ## 8. Manglende evne til at tage beslutninger
> Risko: 4 Impact: 4 <br>
><br>
> Vi må være meget opmærksomme på dette punkt, da det bedømmes at have stor risko og impact. Hvis der er gået over 10 min på at diskutere, stop op, evaluer mulighed efter fordel og ulemper, valg skal tages selvom vi ikke er sikre. 
> Gruppe leder er tiebreaker 

>## 9. Tids konflikt mellem eksamerne 
> Risko: 4 Impact: 3 <br>
><br>
> Vi lægger en plan i god tid, vi holder scopen for vores opgaven rimelig for de ects point vi har. 

> ## 10. Manglende test personer inden for målgruppe 
> Risko: 2 Impact: 2 <br>
><br>
> Vi kontakter folk i god tid, vi kan bruge de studerende på skolen. 

> ## 11. Dødsfald <br>
> Risko:1.5 Impact: 5
> <br>
> Fordi der har været mange dødsfald af pårørende til grupperne, i løbet af de sidste 2 år, er risikoen høj. Der skal være plads til at kunne tage til begravelse og lignende. Gruppen medlemmet forventes at kunne arbejde stadig, på trods af sorgen, hvis dette ikke er muligt bedes gruppe medlemmet om at melde det, og snakke med en studievejleder. 
 