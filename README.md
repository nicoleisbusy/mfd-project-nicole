# CLI Most Uses

> git common command line
``` bash
# change directory 
 cd
# lists
 ls
# back
ctrl c 
# one step back 
 cd ..
```
> git commands every developer should know
``` bash
# add file 
git add ReadMe.txt or git add .
# add everything at once 
git add -A
# commit your changes
git commit -m “your changes”
# push your changes to the remote
git push
#updates/fetch/merge repo 
git pull
# merging your changes
git merge

This operation may cause conflicts that you need to solve manually.

# check status
git status
# navigate into the project root and run
git init
# to see our commit history first use 
 git log --oneline
#undo changes
 git revert

The Git revert command will undo the given commit, but will create a new commit without deleting the older one:

just press shift + q to exit:
```
# Branches

> git branches command line

``` bash
# status of branches
git status
# show all branches
git branch
# switching branches
git switch "branch name"
# puling latest changes
git pull origin
# getting all branches incl remote branches
git branch -r
```
For detailed explanation follow the link
Link: https://www.freecodecamp.org/news/10-important-git-commands-that-every-developer-should-know/#:~:text=6.-,Git%20commit,back%20to%20later%20if%20needed.

