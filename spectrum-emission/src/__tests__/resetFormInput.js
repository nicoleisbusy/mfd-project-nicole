import { mount } from '@vue/test-utils'
//import { resetInput } from '../utillities/resetFormInput.js'
import TheHireUsBlock from '../components/block/TheHireUsBlock'
test('check if form input is reset', async () => {
    const wrapper = mount(TheHireUsBlock)

  await wrapper.find('input[name=from_first_name]').setValue('Lise')
  await wrapper.find('input[name=from_last_name]').setValue('Lisesen')
  await wrapper.find('input[name=from_email]').setValue('Lise@mail.dk')
  await wrapper.find('input[name=subject]').setValue('Lise siger')
  await wrapper.find('textarea[name=message]').setValue('Lise');

await function resetInput() {
      wrapper.find('textarea[name=message]').setValue('');
  };
  

  expect(wrapper.find('textarea[name=message]').element.value).toBe('')
})