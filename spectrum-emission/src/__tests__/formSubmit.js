import { mount } from '@vue/test-utils'
import TheHireUsBlock from '../components/block/TheHireUsBlock'
test('Checks that input value is set', async () => {
  const wrapper = mount(TheHireUsBlock)
  await wrapper.find('textarea[name=message]').setValue('Lise')
  expect(wrapper.find('textarea[name=message]').element.value).toBe('Lise')
})

test('submits a form', async () => {
  const wrapper = mount(TheHireUsBlock)

  await wrapper.find('input[name=from_first_name]').setValue('Lise')
  await wrapper.find('input[name=from_last_name]').setValue('Lisesen')
  await wrapper.find('input[name=from_email]').setValue('Lise@mail.dk')
  await wrapper.find('input[name=subject]').setValue('Lise siger')
  await wrapper.find('textarea[name=message]').setValue('Lise')
  await wrapper.find('input[type=submit]').trigger('click')

  // assert that form has button has been submitted
  await expect(wrapper.emitted('validate'))
})